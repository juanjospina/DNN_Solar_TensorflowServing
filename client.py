from grpc.beta import implementations
import numpy
import tensorflow as tf
from datetime import datetime
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2

tf.app.flags.DEFINE_string('server', 'localhost:9000', 'PredictionService host:port')
FLAGS = tf.app.flags.FLAGS

def _int_feature(value):
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value]))


#def do_inference(hostport):
def do_inference(hostport, Temperature, Prior05Hour):

  """Tests PredictionService with concurrent requests.
  Args:
  hostport: Host:port address of the Prediction Service.
  Returns:
  pred values, ground truth label
  """
  # create connection
  host, port = hostport.split(':')
  channel = implementations.insecure_channel(host, int(port))
  #channel = implementations.insecure_channel('localhost', int(9000))
  stub = prediction_service_pb2.beta_create_PredictionService_stub(channel)

  # initialize a request
  request = predict_pb2.PredictRequest()
  request.model_spec.name = 'example_model'
  #request.model_spec.signature_name = 'prediction'
  request.model_spec.signature_name = 'serving_default'

  # temperature = 24.5
  # prev_power = 3000

  # Get data
  feature_dict = {
        'Temperature': _int_feature(Temperature),
        'Prior05Hour': _int_feature(Prior05Hour)
    }

  example = tf.train.Example(features=tf.train.Features(feature=feature_dict))
  serialized = example.SerializeToString()

  request.inputs['inputs'].CopyFrom(
    tf.contrib.util.make_tensor_proto(serialized, shape=[1]))

    # Predict
  result_future = stub.Predict.future(request, 5.0)
  prediction = result_future.result()

  predicted_rating = prediction.outputs["outputs"].float_val[0]
    #actual_rating = ratings[(ratings['temperature'] == temperature) & (ratings['prev_power'] == prev_power)]['rating'][0]
    #print(f'Predicted value: {predicted_rating} vs actual {actual_rating}')

   # predict
  return predicted_rating

def main(_):

  if not FLAGS.server:
    print('please specify server host:port')
    return

  result = do_inference(FLAGS.server,24,3000)
  print('Result is: ', result)

if __name__ == '__main__':
  tf.app.run()
