# DNN Regressor with ReLu activation functions

# Import Libraries
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import itertools
import tensorflow as tf
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from pylab import rcParams
import matplotlib
import os
import warnings
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

# Start Tensroflow session
sess = tf.InteractiveSession()

#----- Extract Process Training data
train = pd.read_csv('training_testing_data/train.csv')
print('Shape of the train data with all features: ', train.shape)
train = train.select_dtypes(exclude=['object'])
print('Shape of the train data with numerical features: ', train.shape)
train.fillna(0,inplace=True) # Fills the NaN spaces with Zeros

#----- Extract Process Testing Data
test = pd.read_csv('training_testing_data/test.csv')
test = test.select_dtypes(exclude=['object'])
test.fillna(0,inplace=True) # Fills the NaN spaces with Zeros

# Print the list of features available in the dataset
print("List of festures contained in our datasets:", list(train.columns))

# Drop unrequired columns from train and testing dataset
# The commented features are the ones being used being tested for use
train = train.drop('Year', 1)
# train = train.drop('Month', 1)
# train = train.drop('Day', 1)
# train = train.drop('Hour', 1)
# train = train.drop('Minute', 1)
train = train.drop('CloudType', 1)
train = train.drop('DewPoint', 1)
train = train.drop('Pressure', 1)
train = train.drop('WindSpeed', 1)


# Don't drop hour of the testing dataset, so it can be cleaned at the end
# by hour of the day.
test_with_hour = test # dataset before dropping unnecessary features

test = test.drop('Year', 1)
# test = test.drop('Month', 1)
# test = test.drop('Day', 1)
# test = test.drop('Hour', 1)
# test = test.drop('Minute', 1)
test = test.drop('CloudType', 1)
test = test.drop('DewPoint', 1)
test = test.drop('Pressure', 1)
test = test.drop('WindSpeed', 1)

# ----- Preprocessing Data -----
# Rescale our data using the function MinMaxScaler of Scikit-Learn
warnings.filterwarnings('ignore') # Select warnings

col_train = list(train.columns)
col_train_bis = list(train.columns)
col_train_bis.remove('Power') # Remove Target column
num_rows_train = len(train)
mat_y = np.array(train.Power).reshape((num_rows_train,1))

# Min Max procedure
# Create scaler based on the entire dataset available
scaler_y = MinMaxScaler()
scaler_y.fit(mat_y)

# Save a copy of the test dataframe complete before removing Power column
test_complete = test
test = test.drop('Power', 1)

# Scaling Training data
scaler = MinMaxScaler()
train = scaler.fit_transform(train)
#train[['Temperature','PriorDay','PriorWeek','Power']] = scaler.fit_transform(train[['Temperature','PriorDay','PriorWeek','Power']])
train = pd.DataFrame(train,columns=col_train)

# Scaling Testing data
scaler_test = MinMaxScaler()
test = scaler_test.fit_transform(test)
#test[['Temperature','PriorDay','PriorWeek']] = scaler_test.fit_transform(test[['Temperature','PriorDay','PriorWeek']])
test = pd.DataFrame(test,columns=col_train_bis)


# To use Tensorflow we need to tranform our data (features) in a special format
# List of features
COLUMNS = col_train
FEATURES = col_train_bis
LABEL = "Power"

# Columns for Tensorflow
Month = tf.feature_column.numeric_column('Month')
Day = tf.feature_column.numeric_column('Day')
Hour = tf.feature_column.numeric_column('Hour')
Minute = tf.feature_column.numeric_column('Minute')
Temperature = tf.feature_column.numeric_column('Temperature')
PriorDay = tf.feature_column.numeric_column('PriorDay')
PriorWeek = tf.feature_column.numeric_column('PriorWeek')
Seasons = tf.feature_column.numeric_column('Seasons')

# Aggregating the feature columns
feature_cols = [Month, Day, Hour, Minute, Temperature, PriorDay, PriorWeek, Seasons]
#feature_cols = [Temperature, PriorDay, PriorWeek, Seasons]

# Training set and Prediction set with features to predict
training_set = train[COLUMNS]
prediction_set = train.Power

# Divide Train and Test (from training data - test is more like validation set)
# Training 70 %, Test 30 %
x_train, x_test, y_train, y_test = train_test_split(training_set[FEATURES], prediction_set, test_size = 0.2, random_state=42)

# Process for creating Dataframe of training set from "original training set"
y_train = pd.DataFrame(y_train, columns=[LABEL])
training_set = pd.DataFrame(x_train, columns=FEATURES).merge(y_train, left_index=True, right_index=True)

 # Selecting the columns for training (Keeps it the same)
training_sub = training_set[col_train]

# Process for creating Dataframe of testing set from "original training set"
y_test = pd.DataFrame(y_test, columns=[LABEL])
testing_set = pd.DataFrame(x_test, columns=FEATURES).merge(y_test, left_index=True, right_index=True)

#----------- Creating input function -------------
#input_func = tf.estimator.inputs.pandas_input_fn(x = x_train, y = y_train, batch_size = 20, num_epochs = 2000, shuffle=True)
def get_input_fn(x_received, y_received=None, batch_size =20, num_epochs=None, shuffle=True):
  return tf.estimator.inputs.pandas_input_fn(
      x = x_received,
      y = y_received,
      batch_size = batch_size,
      num_epochs=num_epochs,
      shuffle=shuffle)


## ------------ Deep Neural Network for Continuous Features--------##
# Optimizer used is Adagrad (default)
# DNN Model
tf.logging.set_verbosity(tf.logging.ERROR)
tf.logging.set_verbosity(tf.logging.INFO)

regressor = tf.estimator.DNNRegressor(feature_columns=feature_cols, activation_fn=tf.nn.relu, hidden_units=[200,100,50,25,12],optimizer=tf.train.AdagradOptimizer(learning_rate=0.001))
#regressor = tf.estimator.DNNRegressor(feature_columns=feature_cols, activation_fn=tf.nn.relu, hidden_units=[200,200,200,200,200],optimizer=tf.train.AdagradOptimizer(learning_rate=0.001))
#regressor = tf.contrib.learn.DNNRegressor(feature_columns=feature_cols, activation_fn=tf.nn.relu, hidden_units=[200,100,50,25,12])#,optimizer=tf.train.GradientDescentOptimizer(learning_rate=0.1))

# Reset the index of training
# Removes the original Id Index from training_set, starts from 0
training_set.reset_index(drop=True, inplace=True)
#regressor.train(input_fn=get_input_fn(x_train, y_train, batch_size =20000 ,num_epochs=2000, shuffle=True), steps=2000)
regressor.train(input_fn=get_input_fn(x_train, y_train, batch_size =200 ,num_epochs=5000, shuffle=False),steps=50000)
#regressor.fit(input_fn=get_input_fn(x_train, y_train, num_epochs=2000, shuffle=True), steps=2000)

# Evaluation on the test set created by train_test_split
ev = regressor.evaluate(input_fn=get_input_fn(x_test, y_test, num_epochs=1, shuffle=False))
loss_score1 = ev["loss"]# Display the score on the testing set 0.002X in average
print("Final loss on the testing set: {0:f}".format(loss_score1))

# Predictions for the x_test y_test from training data
pred_gen = regressor.predict(input_fn=get_input_fn(x_test, y_test, num_epochs=1, shuffle=False))
predictions = list(pred_gen)

# Final predictions
final_y_preds = []
for pred in predictions:
    final_y_preds.append(pred['predictions'])

predictions = final_y_preds

# #Final RMSE value using DNN Regressor
# from sklearn.metrics import mean_squared_error
# mean_squared_error(y_test, final_y_preds)**0.5


## ---------------- Prepare Data for Visualization and Submission --------------##
# Rescale the predictions to appropiate value range
num_rows_testing_set = len(testing_set)
predictions = scaler_y.inverse_transform(np.array(predictions).reshape(num_rows_testing_set,1))
reality = pd.DataFrame(scaler.inverse_transform(testing_set), columns=[COLUMNS]).Power

# Visualization
matplotlib.rc('xtick', labelsize=10)
matplotlib.rc('ytick', labelsize=10)

fig, ax = plt.subplots(figsize=(15,10))

plt.style.use('ggplot')
plt.plot(predictions, reality, 'ro')
plt.xlabel('Predictions', fontsize=10)
plt.ylabel('Reality', fontsize=10)
plt.title('Predictions x Reality on Dataset Test', fontsize =10)
ax.plot([reality.min(), reality.max()],[reality.min(), reality.max()],'k--',lw=4)
plt.show()

## ---------------- Save data (Submit data) to CSV file --------------##
y_test_complete = pd.DataFrame(test_complete.Power) # Gets the y values for test
test_predictions = regressor.predict(input_fn=get_input_fn(test, y_test_complete, num_epochs=1, shuffle=False))
test_predictions = list(test_predictions)

# Final predictions on the real testing data
final_test_predictions = []
for pred in test_predictions:
    final_test_predictions.append(pred['predictions'])

# Submit Results to a csv file
def to_submit(pred_y,name_out):
    y_predict = pred_y
    y_predict = pd.DataFrame(scaler_y.inverse_transform(np.array(y_predict).reshape(len(y_predict),1)), columns = ['power'])
    y_predict_cleaned = list()

    # cleaning negative numbers and night times (hours)
    for i in range(len(y_predict)):
        if(y_predict.power[i]<0) or (test_with_hour.Hour[i]>=19) or (test_with_hour.Hour[i]<=6):
            y_predict_cleaned.append(0)
        else:
            y_predict_cleaned.append(y_predict.power[i])

    y_predict_cleaned = pd.DataFrame(y_predict_cleaned, columns = ['power'])
    y_predict_cleaned.to_csv(name_out + '.csv',index=False)

# Run the function
to_submit(final_test_predictions, "results_dnn_predict")

# Submit Reality values to csv file to analyze them in Matlab
def to_submit_noscale(pred_y,name_out):
    y_predict = list(itertools.islice(pred_y, test.shape[0]))
    y_predict = pd.DataFrame(y_predict, columns = ['power'])
    y_predict.to_csv(name_out + '.csv',index=False)

# Run the function
to_submit_noscale(test_complete.Power, "results_dnn_real")


# Export Model for Tensorflow Serving
feature_spec = tf.feature_column.make_parse_example_spec(feature_cols)
export_input_fn = tf.estimator.export.build_parsing_serving_input_receiver_fn(feature_spec)
regressor.export_savedmodel('exports', export_input_fn)
